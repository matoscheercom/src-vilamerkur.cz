(function() {
    App = function() {
        'use strict';
        var act = "contactForm";
        var par = "do=";
        
        // scroll to ID main menu
        var setupScrollToId = function() {
            var isAnimatio = false;
            $('.menu .js-scroll').on('click', function(event) {
                event.preventDefault();
                if (isAnimatio == false) {
                    isAnimatio = true;
                    var linkId = $(this).attr('href');
                    $('html, body').animate({scrollTop: $(linkId).offset().top - 65}, 1500, "easeInOutExpo");
                    setTimeout(function() {isAnimatio = false},1500);
                }
            });
        };

        var setupDropdownMenu = function(className) {
            var pathName = window.location.pathname;
            jQuery(className + " .js-sub").hide();
            jQuery(className + " .js-dropdown[href='" + pathName +"']").parents('.sub').show();
            jQuery(className + ' > li .js-dropdown').on('click', function(e){
                e.preventDefault();
                jQuery(this).removeClass("active");
                jQuery(this).addClass("active");

                //var checkElement = $(this).closest('div').attr('class');

                var checkElement = jQuery(this).next();

                if ((checkElement.is(".js-sub")) && (checkElement.is(":visible"))) {
                    checkElement.slideUp(100);
                    jQuery(this).removeClass("active");
                    return false;
                }
                if ((checkElement.is('.js-sub')) && (!checkElement.is(':visible'))) {
                    jQuery(className + ' .js-dropdown').removeClass('active');
                    jQuery(className + ' .js-sub').slideUp(100);
                    checkElement.slideDown(100);
                    jQuery(this).addClass('active');
                    return false;
                }

            });
        };
        
        var setupNavLeftDropdownMenu = function() {
            setupDropdownMenu('.js-nav');
            jQuery('html').on('click', function(event) {
            if( !$(event.target).hasClass('js-target')) {
                jQuery('.js-nav .js-sub').slideUp(100);
                jQuery('.js-nav .js-dropdown').removeClass('active');
                }
            });
        };
        
        
        /** CONTACT SLIDE */
        var ContactScreen = function() {
            var SetupContactForm = function() {
                var contactForm = $('#contact-form');
                var submitButton = $('#contact-form-submit');


                var sub = "submit";

                $('#contact-form input').keydown(function() {
                    contactForm.attr('action', '/?' + par + act + "-" + act + "-" + sub);
                });
                $('#contact-form input').click(function() {
                    contactForm.attr('action', '/?' + par + act + "-" + act + "-" + sub);
                });

                submitButton.click(function(e) {
                    e.preventDefault();
                    contactForm.ajaxSubmit();
                });
            };

            return {
                init: function() {
                    SetupContactForm();
                }
            };
        }();
        
        
        return {
            init: function() {
                $(document).ready(function() {
                    setupScrollToId();
                    setupNavLeftDropdownMenu();
                    ContactScreen.init();
                });
            }
        }
    }();

    App.init();

})();