<?php

use Nette\Application\Routers\RouteList;
use Nette\Application\Routers\Route;

/**
 * Router factory.
 */
class RouterFactory extends Nette\Object {

    /**
     * @return Nette\Application\IRouter
     */
    public function createRouter() {

        $router = new RouteList();

        $router[] = new Route('/[<lang=cs>]', 'Homepage:default');

        $router[] = new Route('[/<lang=cs>]/<presenter>/<action>[/<id>]', 'Homepage:default');
        return $router;
    }
}
